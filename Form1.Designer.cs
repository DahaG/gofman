﻿namespace bloknotGofmanCtroschilowa
{
    partial class Блокнот
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbPole = new System.Windows.Forms.TextBox();
            this.msMenu = new System.Windows.Forms.MenuStrip();
            this.TSMfail = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSozdat = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmOtkrit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmok = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmOkkak = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmParamatr = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmpechat = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmwihod = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMprawka = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmOtmena = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmWirezat = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmkopyr = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmWstawka = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmNaiti = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmNaitiDal = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmzamena = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmPerehod = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmWidelWs = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDateTime = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFormat = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmPPS = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmshrift = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmWid = new System.Windows.Forms.ToolStripMenuItem();
            this.tsnSS = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSprawka = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmPS = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmprogr = new System.Windows.Forms.ToolStripMenuItem();
            this.sfd1 = new System.Windows.Forms.SaveFileDialog();
            this.ofd1 = new System.Windows.Forms.OpenFileDialog();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.msMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbPole
            // 
            this.tbPole.Location = new System.Drawing.Point(-2, 27);
            this.tbPole.Multiline = true;
            this.tbPole.Name = "tbPole";
            this.tbPole.Size = new System.Drawing.Size(617, 529);
            this.tbPole.TabIndex = 0;
            this.tbPole.TextChanged += new System.EventHandler(this.tbPole_TextChanged);
            // 
            // msMenu
            // 
            this.msMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMfail,
            this.TSMprawka,
            this.tsmFormat,
            this.tsmWid,
            this.tsmSprawka});
            this.msMenu.Location = new System.Drawing.Point(0, 0);
            this.msMenu.Name = "msMenu";
            this.msMenu.Size = new System.Drawing.Size(618, 24);
            this.msMenu.TabIndex = 1;
            this.msMenu.Text = "menuStrip1";
            // 
            // TSMfail
            // 
            this.TSMfail.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmSozdat,
            this.tsmOtkrit,
            this.tsmok,
            this.tsmOkkak,
            this.toolStripSeparator1,
            this.tsmParamatr,
            this.tsmpechat,
            this.toolStripSeparator2,
            this.tsmwihod});
            this.TSMfail.Name = "TSMfail";
            this.TSMfail.Size = new System.Drawing.Size(45, 20);
            this.TSMfail.Text = "Файл";
            // 
            // tsmSozdat
            // 
            this.tsmSozdat.Name = "tsmSozdat";
            this.tsmSozdat.Size = new System.Drawing.Size(195, 22);
            this.tsmSozdat.Text = "Создать";
            this.tsmSozdat.Click += new System.EventHandler(this.tsmSozdat_Click);
            // 
            // tsmOtkrit
            // 
            this.tsmOtkrit.Name = "tsmOtkrit";
            this.tsmOtkrit.Size = new System.Drawing.Size(195, 22);
            this.tsmOtkrit.Text = "Открыть...";
            this.tsmOtkrit.Click += new System.EventHandler(this.tsmOtkrit_Click);
            // 
            // tsmok
            // 
            this.tsmok.Name = "tsmok";
            this.tsmok.Size = new System.Drawing.Size(195, 22);
            this.tsmok.Text = "Сохранить";
            this.tsmok.Click += new System.EventHandler(this.tsmok_Click);
            // 
            // tsmOkkak
            // 
            this.tsmOkkak.Name = "tsmOkkak";
            this.tsmOkkak.Size = new System.Drawing.Size(195, 22);
            this.tsmOkkak.Text = "Сохранить как...";
            this.tsmOkkak.Click += new System.EventHandler(this.tsmOkkak_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(192, 6);
            // 
            // tsmParamatr
            // 
            this.tsmParamatr.Name = "tsmParamatr";
            this.tsmParamatr.Size = new System.Drawing.Size(195, 22);
            this.tsmParamatr.Text = "Параметры страницы...";
            // 
            // tsmpechat
            // 
            this.tsmpechat.Name = "tsmpechat";
            this.tsmpechat.Size = new System.Drawing.Size(195, 22);
            this.tsmpechat.Text = "Печать...";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(192, 6);
            // 
            // tsmwihod
            // 
            this.tsmwihod.Name = "tsmwihod";
            this.tsmwihod.Size = new System.Drawing.Size(195, 22);
            this.tsmwihod.Text = "выход";
            this.tsmwihod.Click += new System.EventHandler(this.tsmwihod_Click);
            // 
            // TSMprawka
            // 
            this.TSMprawka.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmOtmena,
            this.toolStripSeparator3,
            this.tsmWirezat,
            this.tsmkopyr,
            this.tsmWstawka,
            this.tsmDelit,
            this.toolStripSeparator4,
            this.tsmNaiti,
            this.tsmNaitiDal,
            this.tsmzamena,
            this.tsmPerehod,
            this.toolStripSeparator5,
            this.tsmWidelWs,
            this.tsmDateTime});
            this.TSMprawka.Name = "TSMprawka";
            this.TSMprawka.Size = new System.Drawing.Size(56, 20);
            this.TSMprawka.Text = "Правка";
            // 
            // tsmOtmena
            // 
            this.tsmOtmena.Name = "tsmOtmena";
            this.tsmOtmena.Size = new System.Drawing.Size(145, 22);
            this.tsmOtmena.Text = "Отменить";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(142, 6);
            // 
            // tsmWirezat
            // 
            this.tsmWirezat.Name = "tsmWirezat";
            this.tsmWirezat.Size = new System.Drawing.Size(145, 22);
            this.tsmWirezat.Text = "Вырезать";
            // 
            // tsmkopyr
            // 
            this.tsmkopyr.Name = "tsmkopyr";
            this.tsmkopyr.Size = new System.Drawing.Size(145, 22);
            this.tsmkopyr.Text = "Копировать";
            // 
            // tsmWstawka
            // 
            this.tsmWstawka.Name = "tsmWstawka";
            this.tsmWstawka.Size = new System.Drawing.Size(145, 22);
            this.tsmWstawka.Text = "Вставить";
            // 
            // tsmDelit
            // 
            this.tsmDelit.Name = "tsmDelit";
            this.tsmDelit.Size = new System.Drawing.Size(145, 22);
            this.tsmDelit.Text = "Удалить";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(142, 6);
            // 
            // tsmNaiti
            // 
            this.tsmNaiti.Name = "tsmNaiti";
            this.tsmNaiti.Size = new System.Drawing.Size(145, 22);
            this.tsmNaiti.Text = "Найти...";
            // 
            // tsmNaitiDal
            // 
            this.tsmNaitiDal.Name = "tsmNaitiDal";
            this.tsmNaitiDal.Size = new System.Drawing.Size(145, 22);
            this.tsmNaitiDal.Text = "Найти далее";
            this.tsmNaitiDal.Click += new System.EventHandler(this.найтиДалеееToolStripMenuItem_Click);
            // 
            // tsmzamena
            // 
            this.tsmzamena.Name = "tsmzamena";
            this.tsmzamena.Size = new System.Drawing.Size(145, 22);
            this.tsmzamena.Text = "Заменить...";
            // 
            // tsmPerehod
            // 
            this.tsmPerehod.Name = "tsmPerehod";
            this.tsmPerehod.Size = new System.Drawing.Size(145, 22);
            this.tsmPerehod.Text = "Перейти...";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(142, 6);
            // 
            // tsmWidelWs
            // 
            this.tsmWidelWs.Name = "tsmWidelWs";
            this.tsmWidelWs.Size = new System.Drawing.Size(145, 22);
            this.tsmWidelWs.Text = "Выделить все";
            // 
            // tsmDateTime
            // 
            this.tsmDateTime.Name = "tsmDateTime";
            this.tsmDateTime.Size = new System.Drawing.Size(145, 22);
            this.tsmDateTime.Text = "Время и дата";
            // 
            // tsmFormat
            // 
            this.tsmFormat.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmPPS,
            this.tsmshrift});
            this.tsmFormat.Name = "tsmFormat";
            this.tsmFormat.Size = new System.Drawing.Size(57, 20);
            this.tsmFormat.Text = "Формат";
            // 
            // tsmPPS
            // 
            this.tsmPPS.Name = "tsmPPS";
            this.tsmPPS.Size = new System.Drawing.Size(169, 22);
            this.tsmPPS.Text = "Перенос по словам";
            this.tsmPPS.Click += new System.EventHandler(this.tsmPPS_Click);
            // 
            // tsmshrift
            // 
            this.tsmshrift.Name = "tsmshrift";
            this.tsmshrift.Size = new System.Drawing.Size(169, 22);
            this.tsmshrift.Text = "Шрифт...";
            this.tsmshrift.Click += new System.EventHandler(this.tsmshrift_Click);
            // 
            // tsmWid
            // 
            this.tsmWid.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsnSS});
            this.tsmWid.Name = "tsmWid";
            this.tsmWid.Size = new System.Drawing.Size(38, 20);
            this.tsmWid.Text = "Вид";
            // 
            // tsnSS
            // 
            this.tsnSS.Name = "tsnSS";
            this.tsnSS.Size = new System.Drawing.Size(166, 22);
            this.tsnSS.Text = "Строка состояния";
            // 
            // tsmSprawka
            // 
            this.tsmSprawka.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmPS,
            this.tsmprogr});
            this.tsmSprawka.Name = "tsmSprawka";
            this.tsmSprawka.Size = new System.Drawing.Size(62, 20);
            this.tsmSprawka.Text = "Справка";
            // 
            // tsmPS
            // 
            this.tsmPS.Name = "tsmPS";
            this.tsmPS.Size = new System.Drawing.Size(183, 22);
            this.tsmPS.Text = "просмотреть справку";
            // 
            // tsmprogr
            // 
            this.tsmprogr.Name = "tsmprogr";
            this.tsmprogr.Size = new System.Drawing.Size(183, 22);
            this.tsmprogr.Text = "о программе";
            // 
            // sfd1
            // 
            this.sfd1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            this.sfd1.FilterIndex = 2;
            this.sfd1.RestoreDirectory = true;
            this.sfd1.FileOk += new System.ComponentModel.CancelEventHandler(this.sfd1_FileOk);
            // 
            // ofd1
            // 
            this.ofd1.FileName = "openFileDialog1";
            this.ofd1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            // 
            // Блокнот
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(618, 559);
            this.Controls.Add(this.tbPole);
            this.Controls.Add(this.msMenu);
            this.MainMenuStrip = this.msMenu;
            this.Name = "Блокнот";
            this.Text = "Блокнот";
            this.msMenu.ResumeLayout(false);
            this.msMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbPole;
        private System.Windows.Forms.MenuStrip msMenu;
        private System.Windows.Forms.ToolStripMenuItem TSMfail;
        private System.Windows.Forms.ToolStripMenuItem TSMprawka;
        private System.Windows.Forms.ToolStripMenuItem tsmFormat;
        private System.Windows.Forms.ToolStripMenuItem tsmWid;
        private System.Windows.Forms.ToolStripMenuItem tsmSprawka;
        private System.Windows.Forms.ToolStripMenuItem tsmSozdat;
        private System.Windows.Forms.ToolStripMenuItem tsmOtkrit;
        private System.Windows.Forms.ToolStripMenuItem tsmok;
        private System.Windows.Forms.ToolStripMenuItem tsmOkkak;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tsmParamatr;
        private System.Windows.Forms.ToolStripMenuItem tsmpechat;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem tsmwihod;
        private System.Windows.Forms.ToolStripMenuItem tsmOtmena;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem tsmWirezat;
        private System.Windows.Forms.ToolStripMenuItem tsmkopyr;
        private System.Windows.Forms.ToolStripMenuItem tsmWstawka;
        private System.Windows.Forms.ToolStripMenuItem tsmDelit;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem tsmNaiti;
        private System.Windows.Forms.ToolStripMenuItem tsmNaitiDal;
        private System.Windows.Forms.ToolStripMenuItem tsmzamena;
        private System.Windows.Forms.ToolStripMenuItem tsmPerehod;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem tsmWidelWs;
        private System.Windows.Forms.ToolStripMenuItem tsmDateTime;
        private System.Windows.Forms.ToolStripMenuItem tsmPPS;
        private System.Windows.Forms.ToolStripMenuItem tsmshrift;
        private System.Windows.Forms.ToolStripMenuItem tsnSS;
        private System.Windows.Forms.ToolStripMenuItem tsmPS;
        private System.Windows.Forms.ToolStripMenuItem tsmprogr;
        private System.Windows.Forms.SaveFileDialog sfd1;
        private System.Windows.Forms.OpenFileDialog ofd1;
        private System.Windows.Forms.FontDialog fontDialog1;
    }
}

