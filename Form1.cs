﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace bloknotGofmanCtroschilowa
{
    public partial class Блокнот : Form
    {

        public Блокнот()
        {
            InitializeComponent();
            tsmPPS.Checked = true;
        }
        string name = "";
        string name1 = "";

        private void toolStripComboBox1_Click(object sender, EventArgs e)
        {

        }

        private void найтиДалеееToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void tsmOkkak_Click(object sender, EventArgs e)
        {
            if (sfd1.ShowDialog() == DialogResult.OK)
            {
                name = sfd1.FileName;
                FileStream file = new FileStream(sfd1.FileName, FileMode.Create, FileAccess.Write);
                StreamWriter fileOut = new StreamWriter(file);
                fileOut.WriteLine(tbPole.Text);
                fileOut.Close();
                file.Close();
            }
        }

        private void sfd1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void tsmwihod_Click(object sender, EventArgs e)
        {
            
            if (tbPole.TextLength != 0)
            {
                MessageBox.Show("Есть несохраненный текст.Сохранить?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            
            if (sfd1.ShowDialog() == DialogResult.OK)
            {
                name = sfd1.FileName;
                FileStream file = new FileStream(sfd1.FileName, FileMode.Create, FileAccess.Write);
                StreamWriter fileOut = new StreamWriter(file);
                fileOut.WriteLine(tbPole.Text);
                fileOut.Close();
                file.Close();
            }
            }

            Close();
        }

        private void tbPole_TextChanged(object sender, EventArgs e)
        {

        }


        private void tsmok_Click(object sender, EventArgs e)
        {
            if (File.Exists(name))
            {
              FileStream file = new FileStream(name, FileMode.Open, FileAccess.Write);
               StreamWriter fileOut = new StreamWriter(file);
                fileOut.WriteLine(tbPole.Text);
                fileOut.Close();
                file.Close();

            }
            else
            {
                tsmOkkak_Click(null, null);
            }

        }

        private void tsmSozdat_Click(object sender, EventArgs e)
        {
            if (tbPole.TextLength != 0)
            {
                MessageBox.Show("Есть несохраненный текст.Сохранить?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            
            if (sfd1.ShowDialog() == DialogResult.OK)
            {
                name = sfd1.FileName;
                FileStream file = new FileStream(sfd1.FileName, FileMode.Create, FileAccess.Write);
                StreamWriter fileOut = new StreamWriter(file);
                fileOut.WriteLine(tbPole.Text);
                fileOut.Close();
                file.Close();
            }
            }
        }

        private void tsmshrift_Click(object sender, EventArgs e)
        {
            fontDialog1.Font = tbPole.Font;
            if (fontDialog1.ShowDialog() != DialogResult.Cancel)
            {
                tbPole.Font = fontDialog1.Font;

            }
        }

        private void tsmPPS_Click(object sender, EventArgs e)
        {
            tsmPPS.Checked = !tsmPPS.Checked;
            if (tsmPPS.Checked==true)
            {
                tbPole.WordWrap = true;
            }
            else
            {
                tbPole.WordWrap = false;
            }
        }

        private void tsmOtkrit_Click(object sender, EventArgs e)
        {
           
            if (ofd1.ShowDialog()==DialogResult.OK)
            {
                name1 = ofd1.FileName;
                FileStream file = new FileStream(name1, FileMode.Open, FileAccess.Read);
                StreamReader fileOut = new StreamReader(file);
                tbPole.Text=fileOut.ReadToEnd();
                fileOut.Close();
                file.Close();


            }
        }
    }
}
